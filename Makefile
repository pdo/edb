
SDVERSION=2.11

# Full ascii listing
ascii:
	perl edb -action 1 > entries.txt

# Full texi listing
texi:
	perl edb -action 11 > entries.texi

# Full LaTeX listing
tex:
	perl edb -action 21 > entries.tex

# HTML files using texi2html
html:
	cp specialmacros2.texi specialmacros.texi
	perl texi2html -split section entries.texi
	perl -pi -e 's/(text\/html)/text\/html;charset=utf-8/' entries*.html

# HTML files using makeinfo
html0:
	cp specialmacros1.texi specialmacros.texi
	makeinfo --html entries.texi
	perl -pi -e 's/(text\/html)/text\/html;charset=utf-8/' entries.html
	perl -pi -e 's/(&#\d+;)%\n/\1/g' entries.html

# DVI from the TeXInfo file
dvi:
	cp specialmacros1.texi specialmacros.texi
	texi2dvi entries.texi

# Postscript from the dvi file
ps:
	dvips -o entries.ps entries.dvi

# PDF file from the texi file
pdf:
	cp specialmacros1.texi specialmacros.texi
	texi2dvi --pdf entries.texi

# The standard (fat) SDIAG files.  Produces the SDAIG directory
sd:
	@if [ "X$(SDVERSION)" = "X" ]; then echo "*** No SDVERSION (e.g. SDVERSION=1.10) ***"; exit 1; fi
	perl edb -action 100 -show_comment 1 -type ULSCMR -ignore_nosdiag 1 -sdiag_version $(SDVERSION)

# The different SDIAG products for the Emacs distribution
sdiag_all:
	@if [ "X$(SDVERSION)" = "X" ]; then echo "*** No SDVERSION (e.g. SDVERSION=1.10) ***"; exit 1; fi
	rm -rf SD_*
	perl edb -action 100 -sdiag_version $(SDVERSION) -types UL 
	mv SDIAG SD_UL
	perl edb -action 100 -sdiag_version $(SDVERSION) -types ULS
	mv SDIAG SD_ULS
	perl edb -action 100 -sdiag_version $(SDVERSION) -types ULSC
	mv SDIAG SD_ULSC
	perl edb -action 100 -sdiag_version $(SDVERSION) -types ULSCMR
	mv SDIAG SD_ULSCMR
	perl edb -action 100 -sdiag_version $(SDVERSION) -types ULMR
	mv SDIAG SD_ULMR
	perl edb -action 100 -sdiag_version $(SDVERSION) -types ULSMR
	mv SDIAG SD_ULSMR
	perl edb -action 100 -sdiag_version $(SDVERSION) -types UMR
	mv SDIAG SD_UMR

# The CPLTAB file for the Emacs distribution
cpl:
	perl edb -action 200 > cpltab

# The etabadd file for the Emacs distribution
etab:
	perl edb -action 500 > etabadd

# The files for Eduardos book.  Zip the stuff immediately.
ekref:
	perl edb -action 300 > reftab.tex
	perl format_errors.pl
	rm -f EK.zip
	zip -r EK.zip EK

# A special edition of the above files which includes all entries and marks
# entries which need further attention.  The is the version Eduardo and I used
# during iteration.  The final book uses the prevous target.
ekrefall:
	perl edb -action 300 -req_49g 1 -req_stack 0 -mark_strange > reftab.tex
	perl format_errors.pl
	rm -f EKall.zip
	zip -r EKall.zip EK

debug4x:
	perl edb -action 4 > Suprom49.stk
	perl edb -action 4 -calc 48G -req_48 1 -req_49 0 > Suprom48.stk
	rm -f debug4x_suprom.zip
	zip debug4x_suprom.zip Suprom49.stk Suprom48.stk

# Most possible products for HP48 and HP49 in 2 separate directories.
all:
	perl makecalc.pl 49G 'HP49G,hp48gII,hp49g+'
	./script
	perl makecalc.pl 48G 'HP48G'
	./script
	perl makecalc.pl 38G 'HP38G'
	./script
	perl makecalc.pl 39G 'HP39G,hp39g+'
	./script
	# The WHATSNEW file
	make whatsnew
	cp WHATSNEW hp49g
	# Make the distribution itself
	make dist

whatsnew:
	perl edb -action 1 -except NAMESVERSION1.0 -subtitle "New entries since 2nd Edition of Programming in System RPL" > WHATSNEW

dist:
	rm -f edb.zip
	zip edb.zip README README.SDIAG entries.db hpedb.el edb Makefile \
            header.tex footer.tex header.texi footer.texi \
            specialmacros1.texi specialmacros2.texi \
            texi2html texinfo.tex \
	    errormessages format_errors.pl \
            SD.DIR


# Install the "make all" products on the web.
install:
	rm -rf ~/public_html/hpcalc/entries/hp49g
	rm -rf ~/public_html/hpcalc/entries/hp48g
	rm -rf ~/public_html/hpcalc/entries/hp38g
	rm -rf ~/public_html/hpcalc/entries/hp39g
	cp -r edb.zip hp48g hp49g hp38g hp39g ${HOME}/public_html/hpcalc/entries


# Remove everything which can be produced again with the database.
clean:
	rm -f entries.texi entries.dvi entries.aux entries.log entries.tmp
	rm -f entries.pg entries.vr entries.cp entries.ky entries.tp
	rm -f entries.fn entries.fns
	rm -f entries.ad  entries.ads
	rm -f entries.toc
	rm -f entries.tex entries.ltx
	rm -f entries.html entries_*.html
	rm -rf SDIAG
	rm -f entries.ps entries.pdf
	rm -rf SD_*
	rm -f entries.txt
	rm -f cpltab etabadd
	rm -rf hp48g hp49g hp38g hp39g
	rm -f *~ .*~ #*#
	rm -rf EK EK.zip EKall.zip reftab.tex
	rm -rf entries.zip edb.zip
