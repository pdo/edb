#!/usr/bin/perl

open OUT,">EK/incl-msgtab.tex" or die "Cannot write to EK/incl-msgtab.tex\n";
select OUT;
open IN,"errormessages" or die "Cannot read error messages\n";

# set the number of messages to include on each page.
$count = 94;
foreach $i (1..60) {push @counts,$count}
$counts[0]=56;

# Initialize count and max.
$cnt = 0;
$max = shift(@counts);

# Read the messages and output them.
while (<IN>) {
  next unless /([0-9A-Z]+) +& +(\S.*\S) *$/;
  ($adr,$msg) = ($1,$2);
  if (defined $msg{$adr}) {die "Duplicate address $n\n"}
  $msg{$adr}=$msg;
  $cnt++;
  if ($cnt > $max) {
    flush_index();
    $cnt = 1;
    $max = shift(@counts);
  }
  add_index_entry($adr,$msg);
}
flush_index();


sub add_index_entry {
  my ($adr,$msg) = @_;
  my $txt;
  $txt = "\\codefnt{$adr} & $msg";
  push @index_collect,$txt;
}

sub flush_index {
  return unless @index_collect;
  # Split into two parts.
  my $n = scalar @index_collect;
  my $l = int($n/2+0.6);
  my @a1 = @index_collect[0..$l-1];
  my @a2 = @index_collect[$l..$#index_collect];
  @index_collect = ();
  # Table header
  print <<'EOH';
\begin{verbatimwrite}{table.tex}
\begin{longtable}{@{}rX|rX@{}}
\textbf{\#n} & \textbf{Message} &
\textbf{\#n} & \textbf{Message} \\ \hline
\endhead
EOH
  # Table entries
  while (@a1) {
    print shift @a1;
    if (@a2) {
      # We do have a right part of the table
      print " & " . shift @a2;
    } else {
      # Only left part of table is being used.
      print " & & ";
    }
    print "\\\\" if @a1;
    print "\n";
  }
  # Table footer.
  print <<"EOF";
\\end{longtable}
\\end{verbatimwrite}
\\begin{footnotesize}
\\LTXtable{\\textwidth}{table.tex}
\\end{footnotesize}
\\newpage

EOF
}

  
