;; hpedb.el --- Major mode for editing the HP48/49 entry database
;; Copyright (c) 1998 Carsten Dominik
;;
;; INSTALLATION
;; ------------
;;
;; In order to get this to work, put the file hpedb.el on your load path
;; (you should compile it) and copy this into your .emacs file:
;;
;;   (autoload 'hpedb-mode "hpedb"       "HP48/49 entry database mode" t)
;;   (setq auto-mode-alist
;;    (append '(("^entries\\.db$" . hpedb-mode))  auto-mode-alist ))
;;
;; To turn hpedb Mode on in a particular buffer, use `M-x hpedb-mode'.
;;
;; COMMENTARY
;; ----------
;; This mode provides font lock support and some helpful functions for
;; the entry database for HP48 and HP49 calculators.
;; ===================================================================
;;
;; TODO:
;; Find entries in address order with next, previous...
;; Select entry is more useful than narrow-to-entry
;;
;; ===========================================================================
(defvar hpedb-stack-column 27
  "Column for the first stack like.")

(defvar hpedb-mode-map (make-sparse-keymap))
(defvar hpedb-font-lock-defaults
  '(
    ;; Comments are lines starting with a star
    ("^\*.*" (0 font-lock-comment-face))
    ;; Keywords lines start with a dod
    ("^\\(\\. +\\S-+:\\)\\(.*\\)"
     (1 font-lock-reference-face) (2 font-lock-type-face))
    ;; Short Description
    ("^\\(,                                                                               \\(\\S-.*\\)?\\)$"
     (1 font-lock-comment-face))
    ;; Description
    ("^\\(;                                                                               \\(\\S-.*\\)?\\)$"
     (1 font-lock-string-face))
    ;; Indented description
    ("^\\(;                                                                               +\\(\\S-.*\\)?\\)$"
     (1 font-lock-variable-name-face))
    ;; Empty description line
    ("^\\(;\\)[ \t]*$"
     (1 font-lock-string-face))
    ;; markup commands
    ("^\\(@\\(chapter\\|section\\|subsection\\|eksubsection\\|subsubsection\\|file\\|starttext\\|endtext\\|input\\).*\\)"
     (1 font-lock-function-name-face))
    ;; Entry name plus start of stack diagram
    ("^\\(\\S-+\\)[ 	][ 	]+\\(\\S-.*\\)" (1 font-lock-keyword-face) (2 font-lock-variable-name-face))
    ;; Extras
    ("^\\(@sdiagextra.*\\)" (1 font-lock-warning-face))
    ;; Entry name without stack diagram
    ("^\\(\\S-+\\)\\s-*$" (1 font-lock-keyword-face))
    ;; Continuation of stack diagram.
    ("^[ 	]+\\(\\S-+.*\\)" (1 font-lock-variable-name-face))
    ;; preprocessor commands
    ("^@\\(\\(iftype\\|ifcalc\\|ifnotek\\|end +if\\).*\\)" (1 font-lock-warning-face))
    ))
     

(defvar hpedb-syntax-table (make-syntax-table))
(modify-syntax-entry ?:  "w" hpedb-syntax-table)
(modify-syntax-entry ?!  "w" hpedb-syntax-table)
(modify-syntax-entry ?@  "w" hpedb-syntax-table)
(modify-syntax-entry ?#  "w" hpedb-syntax-table)
(modify-syntax-entry ?$  "w" hpedb-syntax-table)
(modify-syntax-entry ?%  "w" hpedb-syntax-table)
(modify-syntax-entry ?^  "w" hpedb-syntax-table)
(modify-syntax-entry ?&  "w" hpedb-syntax-table)
(modify-syntax-entry ?\? "w" hpedb-syntax-table)
(modify-syntax-entry ?-  "w" hpedb-syntax-table)
(modify-syntax-entry ?_  "w" hpedb-syntax-table)
(modify-syntax-entry ?=  "w" hpedb-syntax-table)
(modify-syntax-entry ?+  "w" hpedb-syntax-table)
(modify-syntax-entry ?/  "w" hpedb-syntax-table)
(modify-syntax-entry ?<  "w" hpedb-syntax-table)
(modify-syntax-entry ?>  "w" hpedb-syntax-table)
(modify-syntax-entry ?|  "w" hpedb-syntax-table)

(defun hpedb-mode ()
  "Major mode for editing the HP48/49 entries database.

Commands:\\<hpedb-mode-map>
\\[hpedb-stack]    Indent to the stack diagram column.  If already past that column,
           start a stack diagram continuation line.
\\[hpedb-48g]      Insert a line for HP48G entry properties.
\\[hpedb-49g]      Insert a line for HP49G entry properties.
\\[hpedb-userrpl]    Insert a line for a UserRPL name.
\\[hpedb-aka]    Insert a line for AKA
\\[hpedb-short-description]    Insert a line for a short entry description.
\\[hpedb-description]    Insert a line for an entry description.
\\[hpedb-open-line]    Open new line and indent it like the current line.
\\[hpedb-tab]        Reindent line.  This will fix the current line, to ensure that
           - the first stack line starts the stack diagram in column 27
           - subsequent stack start in column 27
           - stack lines starting with an arrow will have the arrow aligned with
             the arrow in the previous line
           - keyword lines have exactly 79 spaces after the dot
           - description lines have at least 79 spaces after the semicolon.
           - sectioning lines have the appropriate \"----\" stuff.

           On headlines (@chapter, @section, @subsection)  cycle
           visibility.  With prefix are and at the beginning of the
           buffer, cycle global visibility.   See the documentation of
           the command hpedb-tab for details.
\\[hpedb-narrow-to-entry]    Narrow buffer to the current entry.
\\[hpedb-widen]    Widen.
\\[hpedb-select-entry]  Select the current entry as a region.
\\[hpedb-kill-entry]    Kill the current entry, can be yanked back.
\\[hpedb-move-entry-up]       Swap this entry and the previous.
\\[hpedb-move-entry-down]     Swap this entry and the following.

\\[hpedb-toggle-case-fold-search]    Toggle the case-fold-search flag.
"
  (interactive)
  (kill-all-local-variables)
  (setq major-mode 'hpedb-mode)
  (setq mode-name "HP-EDB")
  (use-local-map hpedb-mode-map)
  (set-syntax-table hpedb-syntax-table)
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults '(hpedb-font-lock-defaults t))
  (set (make-local-variable 'outline-regexp)
       "@chap\\|@\\(sub\\)*section")
  (set (make-local-variable 'case-fold-search) nil)
  (set (make-local-variable 'fill-column) 125)
  (set (make-local-variable 'font-lock-maximum-size) nil)
  (set (make-local-variable 'indent-tabs-mode) nil)
  (column-number-mode 1)
  (auto-fill-mode 1)
  (if (featurep 'xemacs) (set-frame-width (selected-frame) 135))
  (if (featurep 'xemacs) (filladapt-mode 1))
  (outline-minor-mode)
  (turn-on-font-lock))


(defun hpedb-stack ()
  (interactive)
  (if (> (current-column) 27)
      (insert " \\\n"))
  (indent-to-column 27))

(defun hpedb-aka ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ".")
  (indent-to-column 80)
  (insert "AKA: "))
(defun hpedb-49g ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ".")
  (indent-to-column 80)
  (insert "49G: "))
(defun hpedb-48g ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ".")
  (indent-to-column 80)
  (insert "48G: "))
(defun hpedb-userrpl ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ".")
  (indent-to-column 80)
  (insert "UserRPL: "))
(defun hpedb-short-description ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ",")
  (indent-to-column 80))
(defun hpedb-description ()
  (interactive)
  (hpedb-maybe-newline)
  (insert ";")
  (indent-to-column 80))

(defun hpedb-maybe-newline ()
  (cond
   ((looking-at ".*\\S-.*$")
    (end-of-line 1)
    (newline))
   ((not (bolp))
    (if (string-match "^\\s-*$"
		      (buffer-substring
		       (save-excursion
			 (beginning-of-line 1)
			 (point))
		       (point)))
	(progn
	  (just-one-space)
	  (backward-delete-char 1)
	  )
      (end-of-line 1)
      (newline)))))

(defun hpedb-narrow-to-entry ()
  "Narrow buffer to the current entry"
  (interactive)
  (widen)
  (hpedb-select-entry)
  (narrow-to-region (region-beginning) (region-end)))

(defun hpedb-widen ()
  (interactive)
  (widen))

(defvar hpedb-entry-start-regexp "^[^*<> \t.;]\\|^[<>][^ \t]"
  "Regular expression matching the start of an entry")

(defun hpedb-select-entry ()
  "Select the current entry as a region."
  (interactive)
  (let (beg end)
    (end-of-line 1)
    (if (re-search-forward hpedb-entry-start-regexp nil t)
	(setq end (match-beginning 0))
      (setq end (point-max)))
    (backward-char 1)
    (if (re-search-backward hpedb-entry-start-regexp nil t)
	(setq beg (match-beginning 0))
      (setq beg 1))
    (goto-char end)
    (push-mark beg 'no-msg 'activate)))

(defun hpedb-entry-start ()
  "Move to the start of the entry description.
Also, return its buffer position."
  (cond
   ((looking-at hpedb-entry-start-regexp)
    (point))
   ((re-search-backward hpedb-entry-start-regexp nil t)
    (goto-char (match-beginning 0))
    (point))
   (t
    (error "Could not find beginning of entry"))))

(defun hpedb-entry-end ()
  "Move to the end of the entry description.
Also, return its buffer position."
  (if (re-search-backward hpedb-entry-start-regexp nil t)
      (progn
        (goto-char (match-beginning 0))
        (point))
    (goto-char (point-max))
    (point)))

(defun hpedb-kill-entry ()
  "Kill the current entry.  Will be on clip, so you can yank it again.
Useful for moving an entry."
  (interactive)
  (hpedb-select-entry)
  (kill-region (region-beginning) (region-end)))


(defun hpedb-move-entry-down ()
  "Swap this entry and the next."
  (interactive)
  (hpedb-kill-entry)
  (re-search-forward hpedb-entry-start-regexp nil 'goto)
  (re-search-forward hpedb-entry-start-regexp nil 'goto)
  (beginning-of-line)
  (let ((pos (point)))
    (yank)
    (goto-char pos)))

(defun hpedb-move-entry-up ()
  "Swap this entry and the previous."
  (interactive)
  (hpedb-kill-entry)
  (backward-char 1)
  (re-search-backward hpedb-entry-start-regexp nil 'goto)
  (beginning-of-line)
  (let ((pos (point)))
    (yank)
    (goto-char pos)))

(defun hpedb-toggle-case-fold-search ()
  "Toggle the case-fold-search flag."
  (interactive)
  (setq case-fold-search (not case-fold-search))
  (message "Case folding is %s"
	   (if case-fold-search "on" "off")))

(defun hpedb-indent-line ()
  "Reindent current line, make sure the right amount of space is present."
  (interactive)
  (let (goto)
    (save-excursion
      (beginning-of-line 1)
      (cond
       ;; comment lines
       ((looking-at "\\*"))
       
       ;; preprocessore lines
       ((looking-at "\\( *\\)@\\(if\\|end\\)"))
       
       ;; section lines etc
       ((looking-at "@\\(chapter\\|section\\|subsection\\|eksubsection\\|file\\|input\\)")
	(when (member (match-string 1) '("chapter" "section" "subsection"))
	  (let* ((type (match-string 1))
		 (char (cdr (assoc type
				   '(("chapter" . ?=)
				     ("section" . ?-)
				     ("subsection" . ?-)))))
		 (n 0))
	    (if (looking-at ".*[^-=]\\(=+\\|-+\\) *$")
		(delete-region (match-beginning 1) (match-end 0)))
	    (if (looking-at ".*[^ \n]\\( +\\)$")
		(delete-region (match-beginning 1) (match-end 0)))
	    (end-of-line)
	    (setq n (- fill-column (current-column)))
	    (insert " " (make-string (max n 0) char)))))
	
       ;; keyword lines
       ((looking-at "\\. *")
	(replace-match ".                                                                               "))
       
       ;; descriptions lines
       ((and (looking-at "\\([,;]\\)\\( +\\)\\(\\S-\\)")
	     (< (length (match-string 1)) 79))
	(replace-match (concat
                        (match-string 1)
			"                                                                               "
			(match-string 3))
		       'fixed 'literal)
	(setq goto (1- (point))))
       
       ;; Stack diagrams, first line
       ((and (looking-at "\\([^ ;.\n]\\S-*\\)\\( +\\)\\(\\S-\\)")
	     (not (= (- (match-end 2) (match-beginning 1)) hpedb-stack-column)))
	(replace-match (concat (match-string 1)
			       (make-string (- hpedb-stack-column (length (match-string 1))) ?\ )
			       (match-string 3))
		       'fix 'literal)
	(setq goto (1- (point))))
       
       ;; Stack diagram line, but currently no stack diagram
       ((and (looking-at "\\([^ ;.\n]\\S-*\\)\\( *\\)$")
	     (not (= (- (match-end 2) (match-beginning 1)) hpedb-stack-column)))
	(replace-match (concat (match-string 1)
			       (make-string (- hpedb-stack-column (length (match-string 1))) ?\ ))
		       'fix 'literal)
	(setq goto (1- (point))))

       ;; Stack diagrams, subsequent lines starting with arrow
       ((looking-at "\\( +\\)\\(\\\\->.*\\)")
	(let* ((col1 (- (match-beginning 2) (match-beginning 0)))
	       (ind (length (match-string 1)))
	       col0 ndel)
	  (save-excursion
	    (beginning-of-line 0)
	    (save-match-data
	      (if (looking-at "\\(.*\\(\\\\->\\).*\\)")
		  (setq col0 (- (match-beginning 2) (match-beginning 0))))))
	  (when col0
	    (setq ndel (- col1 col0))
	    (if (< (- ind ndel) hpedb-stack-column)
		(setq ndel (- ind hpedb-stack-column)))
	    (if (> ndel 0)
		(delete-char ndel)
	      (insert (make-string (- ndel) ?\ ))))))
       
       ;; Stack diagrams, lines not starting with an arrow
       ((looking-at "\\( +\\)\\(\\S-\\)")
	(replace-match (concat (make-string hpedb-stack-column ?\ ) (match-string 2))
		       'fixed 'literal)
	(setq goto (1- (point))))))
    (and goto (goto-char goto))))

(defun hpedb-open-line ()
  "Open a new line and intent is like the current line."
  (interactive)
  (insert "\n")
  (if (looking-at " +") (replace-match ""))
  (beginning-of-line 0)
  (if (looking-at "[.;]? +")
      (progn
	(beginning-of-line 2)
	(insert (match-string 0)))
    (beginning-of-line 2)))

(defun hpedb-sections ()
  (interactive)
  (occur "^@\\(chapter\\|section\\|subsection\\){"))


(defvar hpedb-cycle-global-status nil)
(defvar hpedb-cycle-subtree-status nil)
(defun hpedb-tab (&optional arg)
  "Visibility cycling for outline(-minor)-mode.

- When this function is called with a prefix argument, rotate the entire
  buffer through 3 states (global cycling)
  1. OVERVIEW: Show only top-level headlines.
  2. CONTENTS: Show all headlines of all levels, but no body text.
  3. SHOW ALL: Show everything.

- When point is at the beginning of a headline, rotate the subtree started
  by this line through 3 different states (local cycling)
  1. FOLDED:   Only the main headline is shown.
  2. CHILDREN: The main headline and the direct children are shown.  From
               this state, you can move to one of the children and
               zoom in further.
  3. SUBTREE:  Show the entire subtree, including body text.

- When point is not at a headline, execute hpedb-indent-line in order to
indent and format the current line.

- Special case: if point is the the beginning of the buffer and there is
  no headline in line 1, this function will act as if called with prefix arg."
  (interactive "P")

  (if (and (bobp) (not (looking-at outline-regexp)))
      ; special case:  use global cycling
      (setq arg t))

  (cond

   (arg ;; Global cycling

    (cond
     ((and (eq last-command this-command)
	   (eq hpedb-cycle-global-status 'overview))
      ;; We just created the overview - now do table of contents
      ;; This can be slow in very large buffers, so indicate action
      (message "CONTENTS...")
      (save-excursion
	;; Visit all headings and show their offspring
	(goto-char (point-max))
	(catch 'exit
	  (while (and (progn (condition-case nil
				 (outline-previous-visible-heading 1)
			       (error (goto-char (point-min))))
			     t)
		      (looking-at outline-regexp))
	    (show-branches)
	    (if (bobp) (throw 'exit nil))))
	(message "CONTENTS...done"))
      (setq hpedb-cycle-global-status 'contents))
     ((and (eq last-command this-command)
	   (eq hpedb-cycle-global-status 'contents))
      ;; We just showed the table of contents - now show everything
      (show-all)
      (message "SHOW ALL")
      (setq hpedb-cycle-global-status 'all))
     (t
      ;; Default action: go to overview
      (hide-sublevels 1)
      (message "OVERVIEW")
      (setq hpedb-cycle-global-status 'overview))))

   ((save-excursion (beginning-of-line 1) (looking-at outline-regexp))
    ;; At a heading: rotate between three different views
    (outline-back-to-heading)
    (let ((goal-column 0) beg eoh eol eos nxh)
      ;; First, some boundaries
      (save-excursion
	(outline-back-to-heading)  (setq beg (point))
	(save-excursion
	  (beginning-of-line 2)
	  (while (and (not (eobp))   ;; this is like `next-line'
		      (get-char-property (1- (point)) 'invisible))
	    (beginning-of-line 2)) (setq eol (point)))
	(outline-end-of-heading)   (setq eoh (point))
	(outline-end-of-subtree)   (setq eos (point))
        (outline-next-heading)     (setq nxh (point)))
      ;; Find out what to do next and set `this-command'
      (cond
       ((= eos eoh)
	;; Nothing is hidden behind this heading
        (message "EMPTY ENTRY")
        (setq hpedb-cycle-subtree-status nil))
       ((>= eol eos)
	;; Entire subtree is hidden in one line: open it
	(show-entry)
	(show-children)
	(message "CHILDREN")
	(setq hpedb-cycle-subtree-status 'children))
       ((and (eq last-command this-command)
	     (eq hpedb-cycle-subtree-status 'children))
	;; We just showed the children, now show everything.
	(show-subtree)
	(message "SUBTREE")
	(setq hpedb-cycle-subtree-status 'subtree))
       (t
	;; Default action: hide the subtree.
	(hide-subtree)
	(message "FOLDED")
	(setq hpedb-cycle-subtree-status 'folded)))))

   ;; TAB emulation
   (t (hpedb-indent-line))))

(defun hpedb-at-headline-p ()
  (save-excursion (beginning-of-line 1) (looking-at outline-regexp)))

(define-key hpedb-mode-map "\C-c8"    'hpedb-48g)
(define-key hpedb-mode-map "\C-c9"    'hpedb-49g)
(define-key hpedb-mode-map "\C-c\C-u" 'hpedb-userrpl)
(define-key hpedb-mode-map "\C-c\C-a" 'hpedb-aka)
(define-key hpedb-mode-map "\C-c\C-s" 'hpedb-stack)
(define-key hpedb-mode-map "\C-c\C-n" 'hpedb-narrow-to-entry)
(define-key hpedb-mode-map "\C-c\C-w" 'hpedb-widen)
(define-key hpedb-mode-map "\C-c "    'hpedb-select-entry)
(define-key hpedb-mode-map "\C-c\C-k" 'hpedb-kill-entry)
(define-key hpedb-mode-map [(control up)] 'hpedb-move-entry-up)
(define-key hpedb-mode-map [(control down)] 'hpedb-move-entry-down)
(define-key hpedb-mode-map "\C-c\C-f" 'hpedb-toggle-case-fold-search)
(define-key hpedb-mode-map "\C-c\C-o" 'hpedb-open-line)
(define-key hpedb-mode-map "\C-c,"    'hpedb-short-description)
(define-key hpedb-mode-map "\C-c\C-e" 'hpedb-short-description)
(define-key hpedb-mode-map "\C-c;"    'hpedb-description)
(define-key hpedb-mode-map "\C-c\C-d"    'hpedb-description)
(define-key hpedb-mode-map "\C-i"     'hpedb-tab)
(define-key hpedb-mode-map "\C-c="    'hpedb-sections)
(define-key hpedb-mode-map [(shift right)] 'hpedb-next-entry)
(define-key hpedb-mode-map [(shift left)] 'hpedb-previous-entry)

(easy-menu-define hpedb-menu hpedb-mode-map "HPEDB menu"
  '("Entries"
    "Add database field"
    ["48G" hpedb-48g t]
    ["49G" hpedb-49g t]
    ["UserRPL" hpedb-49g t]
    ["AKA" hpedb-aka t]
    ["Indent to Stack Column" hpedb-stack t]
    ["Short Description" hpedb-short-description t]
    ["Description" hpedb-description t]
    "--"
    ["Re-indent" hpedb-tab (not (hpedb-at-headline-p))]
    ["Open line" hpedb-open-line t]
    "--"
    "Work on Entry"
    ["Narrow to entry" hpedb-narrow-to-entry t]
    ["Widen" hpedb-widen t]
    ["Select Entry" hpedb-select-entry t]
    ["Kill entry" hpedb-kill-entry t]
    ["Move entry up" hpedb-move-entry-up t]
    ["Move entry down" hpedb-move-entry-down t]
    "--"
    "Address Order"
    ["Next" hpedb-next-entry t]
    ["Previous" hpedb-previous-entry t]
    "--"
    "Document Overview"
    ["Cycle Outline Visibility" hpedb-tab (or (bobp)
                                              (hpedb-at-headline-p))]
    ["Occur sections" hpedb-sections t]
    "--"
    "Options"
    ["Case-fold-search" hpedb-toggle-case-fold-search
     :style toggle :selected case-fold-search]))

(provide 'hpedb)

;; FIXME: Move to better place.
(defun hpedb-previous-entry (arg)
  (interactive "P")
  (hpedb-next-entry (not arg)))
(defun hpedb-next-entry (arg)
  (interactive "P")
  (catch 'exit
    (let ((min 0)
          (pos (point))
          (max (string-to-number "FFFFFF" 16))
          address-list member
          address naddress calc lib n fmt re)
      (beginning-of-line 1)
      (re-search-forward "^\\. +\\([0-9][^:]+\\): *\\([0-9A-H]+\\)" nil t)
      (setq calc (match-string 1)
            address (match-string 2)
            address-list (hpedb-get-addresses calc)
            member (member address address-list)
            naddress (if (not arg)
                         (car (cdr member))
                       (car (nthcdr (1- (- (length address-list) (length member))) address-list))))
      (message naddress)
      (goto-char (point-min))
      (setq re (concat "^\\. +" (regexp-quote calc) ": *\\(" naddress "\\>\\)"))
      (if (re-search-forward re nil t)
          (progn
            (goto-char (match-beginning 1))
            (throw 'exit t))
        (goto-char pos)
        (error "No further entries")))))

(defvar hpedb-address-cache nil)
(defvar hpedb-last-address-calc "")
(defun hpedb-get-addresses (calc &optional force-update)
  (if (and (not force-update)
           (equal calc hpedb-last-address-calc))
      nil
    (message "Caching address list...")
    (setq hpedb-last-address-calc calc
          hpedb-address-cache
          (hpedb-all-addresses calc))
    (message "Caching address list...done"))
  hpedb-address-cache)

(defun hpedb-all-addresses (calc)
  (let ((re (concat  "^\\. +" (regexp-quote calc) ": *\\(\\S-+\\)"))
        list adr l rtn)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward re nil t)
        (setq adr (match-string-no-properties 1))
        (setq list 
              (cons
               (cons
                (string-to-number
                 (if (= (length adr) 6)
                     (concat "1" (substring adr 3) (substring adr 0 3))
                   adr)
                 16)
                adr)
               list))))
    (setq list (mapcar 'cdr (sort list 'hpedb-compare-addresses)))
    ;; Remove double entries
    (while (setq l (car list))
      (if (not (equal (car rtn) l))
          (setq rtn (cons l rtn)))
      (setq list (cdr list)))
    (nreverse rtn)))

(defun hpedb-compare-addresses (a b)
  (< (car a) (car b)))


